package Sample;

import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.*;

/**
 * @author Girikumar Sayana
 *
 */
public class CreateRecord {
	private WebDriver driver;
	private String baseUrl;
	private WebDriverWait driverWait;
	
	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/src/test/resources/chromedriver.exe");
		driver = new ChromeDriver();
		baseUrl = "https://app.iformbuilder.com";
	}
	
	@Test
	public void Test() {		
		driver.get(baseUrl+"/exzact/dataViews.php");
		driver.manage().window().maximize();
		this.driverWait = new WebDriverWait(driver, 30);	
		
		//Login
		signInWith("girikumar.sayana@gmail.com","Kumarsayana9^");
		
		//Select Form with the label
		String list_label = "My demo";	
		String list_id = SelectForm(list_label);
		
		//Create New record
		ClickCreateNewRecord(driver);
		
		char[] chara =  list_id.toCharArray();
		int i;
		for( i=0 ; i< list_id.length(); i++){
			if(Character.isDigit(chara[i])){
				break;}}
		
		//Create New record item.
		String listIdNum = list_id.substring(i);
		
		String userFirstname = "Girikumar";
		String userLastname = "sayana";
		String userPhonenumber = "2343529437";
		CreateNewRecord(listIdNum, userFirstname, userLastname, userPhonenumber);
		
		//Verify list is created
		VerifyRecordCreated(userFirstname, userLastname, userPhonenumber);
	
	}
	
		public void signInWith(String userName, String password) {
		isElementPresent(By.name("login"));
		
		//Enter Username and Password
		driver.findElement(By.name("USERNAME")).sendKeys(userName);
		driver.findElement(By.name("PASSWORD")).sendKeys(password);
		
		//click the login button
		WebElement loginBtn = driver.findElement(By.id("login")).findElement(By.tagName("input"));			
		loginBtn.click();
	}
	
	public String SelectForm(String list_label){
		String list_id = driver.findElement(By.xpath("//div[@class='bDivBox']//td/div[contains(text(),'"+list_label+"')]//ancestor::tr")).getAttribute("id");		
		WebElement list = driver.findElement(By.xpath("//div[@class='bDivBox']/table//td[@id='"+list_id+"_ID']"));
		list.click();
		return list_id;
	}
	
	public void ClickCreateNewRecord(WebDriver driver){
		WebElement newRecord = driver.findElement(By.xpath("//div[@class='tDiv']//div[@id='fbutton_Create_New_Record']"));
		newRecord.click();
	}
	
	public void VerifyRecordCreated(String userFirstname, String userLastname,String userPhonenumber)
	{
		//WebElement VerifyFirstName;
		isElementPresent(By.xpath("//table[@id='flex1']//td/div[.='"+userFirstname+"']"));		
		isElementPresent(By.xpath("//table[@id='flex1']//td/div[.='"+userLastname+"']"));
		isElementPresent(By.xpath("//table[@id='flex1']//td/div[.='"+userPhonenumber+"']"));	
	}
	
	public void CreateNewRecord(String listIdNum, String userFirstname, String userLastname, String userPhonenumber)
	{
		isElementPresent(By.xpath(
				"//div[@id='datatable']//table//div[.='First name']/../following-sibling::td//input[@id='p"+listIdNum+"_rec0_first_name']"));
		
		WebElement firstnameTxt = driver.findElement(By.xpath(
				"//div[@id='datatable']//table//div[.='First name']/../following-sibling::td//input[@id='p"+listIdNum+"_rec0_first_name']"));
		firstnameTxt.sendKeys(userFirstname);
		
		WebElement lastnameTxt = driver.findElement(By.xpath(
				"//div[@id='datatable']//table//div[.='Last Name']/../following-sibling::td//input[@id='p"+listIdNum+"_rec0_last_name']"));
		lastnameTxt.sendKeys(userLastname);
		
		WebElement numberTxt = driver.findElement(By.xpath(
				"//div[@id='datatable']//table//div[.='Phone Number']/../following-sibling::td//input[@id='p"+listIdNum+"_rec0_phone_number']"));
		numberTxt.sendKeys(userPhonenumber);
		
		WebElement saveBtn = driver.findElement(By.xpath("//div[@id='custombtn_list']//a[.='Save']"));
		saveBtn.click();
	}
	
	protected boolean isElementPresent(By by){
        try{
        	driverWait.until(ExpectedConditions.visibilityOfElementLocated(by));
            driver.findElement(by);            
            return true;
        }
        catch(NoSuchElementException e){
            return false;
        }
    }	

	@After
	public void tearDown() throws Exception {
	    driver.quit();
	}
}
