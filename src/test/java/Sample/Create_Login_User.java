package Sample;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.*;

/**
 * @author Girikumar Sayana
 *
 */
public class Create_Login_User {
	
	private WebDriver driver;
	WebDriverWait driverWait ;
	private String baseUrl;
	
	@Before
	public void setUp() {
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/src/test/resources/chromedriver.exe");
		driver = new ChromeDriver();
		baseUrl = "https://app.iformbuilder.com";
	}
	
	@Test
	public void Test() {		
		driver.get(baseUrl+"/exzact/adminUsers.php");
		driver.manage().window().maximize();	
		driverWait = new WebDriverWait(driver, 10);
		
		
		String username1 = "girikumar.sayana@gmail.com";
		String username2 = "giri.sayana@gmail.com";
		String user1Password = "Kumarsayana9^";
		
		//login user1
		LoginUser(username1, user1Password);
		
		//Verify username with the top left corner
		VerifyLoggedInUser(username1);
		
		//CLick on new user button
		driver.findElement(By.id("fbutton_New")).click();
		
		//Enter the New User Details.		
		String user2Password = "Kumarsayana9^";		
		EnterNewUserDetails(username2,"Giri", "sayanaa", "giri.sayana@gmail.com", user2Password);
		
		//verification of user List with username
		String userlistItemXpath = "//div/a[contains(text(),'"+username2+"')]";
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(userlistItemXpath)));
		WebElement userlist = driver.findElement(By.xpath(userlistItemXpath));
		if(!userlist.isDisplayed())
		{
			System.out.println(username2+": user is Not Found");
		}
		
		//Logout current user.
		WebElement logoutBtn = driver.findElement(By.xpath("//a[@href='logout.php']"));
		logoutBtn.click();
		
		//Login with new User
		LoginUser(username2,user2Password);
		//Verify username
		VerifyLoggedInUser(username2);
		
	}
	
	
	public void EnterNewUserDetails(String username,String firstname,String lastname,String email,String password)
	{		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.name("newUsername")));
		driver.findElement(By.name("newUsername")).sendKeys(username);
		driver.findElement(By.name("newLastname")).sendKeys(firstname);
		driver.findElement(By.name("newFirstname")).sendKeys(lastname);
		driver.findElement(By.name("newEmail")).sendKeys(email);
		driver.findElement(By.name("newPassword")).sendKeys(password);
		driver.findElement(By.name("newPasswordAgain")).sendKeys(password);
		
		if ( !driver.findElement(By.name("newCreateRight")).isSelected() )
		{
		     driver.findElement(By.name("newCreateRight")).click();
		}
		
		driver.findElement(By.xpath("//div[@id='custombtn_list']//a[contains(text(),'Create User')]")).click();		
	}
	
	public void LoginUser(String username, String password)
	{		
		driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.name("login")));
		driver.findElement(By.name("login"));			
		
		//Enter Username and Password		
		WebElement txtUsername = driver.findElement(By.name("USERNAME"));
		txtUsername.click();
		txtUsername.sendKeys(username);
		
		WebElement txtPassword = driver.findElement(By.name("PASSWORD"));
		txtPassword.click();
		txtPassword.sendKeys(password);
		
		//click the login button
		WebElement loginBtn = driver.findElement(By.id("login"));
		loginBtn.findElement(By.tagName("input")).click();		
	}
	
	public void VerifyLoggedInUser(String username)
	{
		//Verify username with the top left corner
		String loggedUser = driver.findElement(By.className("loggedin")).getText();		
		Assert.assertTrue(loggedUser.contains(username));
	}
	
	 @After
	  public void tearDown() throws Exception {
	    driver.quit();	   
	  }
	
}
